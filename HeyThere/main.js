/*
Hey there.
Du willst also wissen wie die Knuddels Apps eigentlich funktionieren?
Lies ruhig weiter, dann siehst du wie einfach das eigentlich alles ist.
*/


// Jede deiner Apps benötigt eine Variable namens App. Wenn die fehlt wird deine App nichts machen können.
var App = {}

	
// Eine App hat viele Funktionen die automatisch ausgeführt werden. Eine davon ist diese hier. Sobald ein User deinen Channel betritt wird sie ausgeführt und übergibt den User der den Channel betreten hat.
App.onUserJoined = function(user)
{		
	// Hier definieren wir eine Variable namens message, die den neuen User mit seinem Nickname begrüßt und ihm sagt wie alt er ist. 
	// Aus einem User Objekt kannst du noch viel mehr Daten auslesen. Zum Beispiel auch wann sich der User registriert hat. 
	var message = 'Hey ' + user.getNick() + ', willkommen im Channel. Du bist ' + user.getAge() + ' Jahre alt.';
		
		
	// Jetzt senden wir von deinem Appbot aus eine private Nachricht an den neuen User. 
	user.sendPrivateMessage(message);
		
		
}

