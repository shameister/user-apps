/*
Hey there. Na, hast du die Aufgabe alleine geschafft? 
Falls nein, ist das nicht so schlimm. 
Sieh dir an wie ich es gemacht habe und versuch es einfach dann selbst.
Wir sehen uns wieder im Tutorial.
Bis gleich! 
*/

var App = {}

App.onUserJoined = function(user)
{		

	var gender = user.getGender();
	
	// Lass uns noch eine Variable namens age definieren, in welcher wir das Alter des neuen Users ablegen. 
	var age = user.getAge();
	
	var nick = user.getNick();
	
	var genderGreeting;
	
	
	if (gender == Gender.Male)
	{
		// Hier überprüfen wir ob der neue User jünger als 18, zwischen 18 und 60 oder älter als 60 Jahre alt ist.
		if(age < 18)
		{
			genderGreeting = 'Junge ' + nick;
		}
		else if (age < 61)
		{
			genderGreeting = 'Herr ' + nick;			
		}
		else
		{
			genderGreeting = 'Opa ' + nick;			
		}
	}
	else if (gender == Gender.Female)
	{
		// Hier überprüfen wir ob der neue User jünger als 18, zwischen 18 und 60 oder älter als 60 Jahre alt ist.
		if(age < 18)
		{
			genderGreeting = 'Mädchen ' + nick;	
		}
		else if (age < 61)
		{
			genderGreeting = 'Frau ' + nick;	
		}
		else
		{
			genderGreeting = 'Oma ' + nick;	
		}
	}
	else
	{
		// Hier überprüfen wir ob der neue User jünger als 18, zwischen 18 und 60 oder älter als 60 Jahre alt ist.
		// Da wir für diesen Fall das Geschlecht des Users nicht wissen, nennen wir ihn einfach Kind, Erwachsener oder Rentner. 
		if(age < 18)
		{
			genderGreeting = 'Kind ' + nick;	
		}
		else if (age < 61)
		{
			genderGreeting = 'Erwachsener ' + nick;	
		}
		else
		{
			genderGreeting = 'Rentner ' + nick;	
		}	
	}
	
	var message = 'Hey ' + genderGreeting +  ', willkommen im Channel. Du bist ' + age + ' Jahre alt.';
	user.sendPrivateMessage(message);		
}