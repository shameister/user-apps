# README #

Ahoi,

cool, dass du hier gelandet bist. User Apps für Knuddels MyChannel zu schreiben macht viel Spaß!

Momentan sind wir gerade dabei die API richtig aufzusetzen und immer mehr Entwickler zuzulassen.

Im [Projekt-Wiki](https://bitbucket.org/knuddels/user-apps/wiki) findest du viele wichtige Informationen dazu, wie du am besten startest.

### Wofür ist dieses Repository? ###

* Beispiel-Code für User Apps
* Lernen, wie man User Apps baut


### Wie kann ich mitwirken? ###

* Schreibe eigene User Apps für Knuddels
* Verbessere [unsere Beispiel-Apps](https://bitbucket.org/knuddels/user-apps/src) und erstelle [Pull-Requests](https://bitbucket.org/knuddels/user-apps/pull-requests)
* Aktualisiere das [Wiki](https://bitbucket.org/knuddels/user-apps/wiki/Home)

### Mit wem muss ich reden? ###

* Die beste Art Kontakt aufzunehmen ist eine /m (nicht /p ;-)) im Chat an Ironist, in der du genau sagst, was du dir vorstellst und was deine Fragen sind. :-)